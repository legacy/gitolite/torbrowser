1. Install Debian squeeze (latest stable) on a VM from:
   http://cdimage.debian.org/debian-cd/6.0.6/i386/iso-cd/debian-6.0.6-i386-netinst.iso
   # Make sure you have 10gb available.
   # Make sure you install the desktop environment since it gets a lot of the 
   # necessary X11 dependencies.
 
2. sudo aptitude install git 

3. git clone https://git.torproject.org/torbrowser.git 

4. cd torbrowser/build-scripts

5. vi linux-alpha.mk
   # change FETCH_DIR to be something you have access to
   # like $(HOME)/srv/build-trees

6. Firefox needs a version of yasm only available in Debian unstable:
   a. edit /etc/apt/sources.list, change 'squeeze' to 'unstable'
   b. sudo aptitude update && sudo aptitude install yasm
   c. edit /etc/apt/sources.list and change 'unstable' back to 'squeeze'
   d. sudo aptitude update again so you'll only install packages from squeeze 
 
10. sudo aptitude build-dep iceweasel
 
11. sudo aptitude install zlib1g-dev g++ libxext-dev cmake autoconf2.13 mesa-common-dev tcl8.5 tardy
 
12. make -f linux-alpha.mk build-all-binaries 

13. make -f linux-alpha.mk compressed-bundle_en-US # or all-bundles


To clean the bundle directory (which includes the XPIs, but not the
compiled binaries):
  make -f linux-alpha.mk clean

If you need to rebuild selective components, you can try various clean
targets. For example, to clean firefox:
  make -f linux-alpha.mk clean-build-firefox
