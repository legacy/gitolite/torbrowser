#!/bin/bash
#
# Creates a local mirror of all third party sources. Verifies GPG signatures
# when possible.
#
# Usage:
# ./fetch-thirdparty.sh [<dir>]
#
# Downloads sources into a "sources" subdirectory of the current directory.
#
# If <dir> is specified, we change directory into that mirror dir before
# downloading source.

. ./versions.sh

if [ -d $1 ]; then
  cd $1
fi

## Location of files for download
ZLIB_URL=http://www.zlib.net/${ZLIB_PACKAGE}
OPENSSL_URL=http://www.openssl.org/source/${OPENSSL_PACKAGE}
LIBPNG_URL=ftp://ftp.simplesystems.org/pub/libpng/png/src/libpng15/${LIBPNG_PACKAGE}
QT_URL=ftp://ftp.qt.nokia.com/qt/source/${QT_PACKAGE}
VIDALIA_URL=https://archive.torproject.org/tor-package-archive/vidalia/${VIDALIA_PACKAGE}
LIBEVENT_URL=https://github.com/downloads/libevent/libevent/${LIBEVENT_PACKAGE}
TOR_URL=https://archive.torproject.org/tor-package-archive/${TOR_PACKAGE}
TOR_ALPHA_URL=https://archive.torproject.org/tor-package-archive/${TOR_ALPHA_PACKAGE}
PIDGIN_URL=http://sourceforge.net/projects/pidgin/files/Pidgin/${PIDGIN_PACKAGE}
FIREFOX_URL=https://ftp.mozilla.org/pub/mozilla.org/firefox/releases/${FIREFOX_VER}/source/${FIREFOX_PACKAGE}
MOZBUILD_URL=https://ftp.mozilla.org/pub/mozilla.org/mozilla/libraries/win32/${MOZBUILD_PACKAGE}
TORBUTTON_URL=https://people.torproject.org/~mikeperry/${TORBUTTON_PACKAGE}
HTTPSE_URL=https://www.eff.org/files/${HTTPSE_PACKAGE}
HTTPSE_DEV_URL=https://www.eff.org/files/${HTTPSE_DEV_PACKAGE}
OBFSPROXY_URL=https://archive.torproject.org/tor-package-archive/obfsproxy/${OBFSPROXY_PACKAGE}

# XXX: Because these filenames aren't versioned, we just fetch them directly
# from mozilla during build..
#NOSCRIPT_URL=https://addons.mozilla.org/firefox/downloads/latest/722/${NOSCRIPT_PACKAGE}
#PDFJS_URL=https://addons.mozilla.org/firefox/downloads/latest/352704/${PDFJS_PACKAGE}

if [ ! -d sources ]; then
  mkdir sources
fi

cd sources

# Get package files
for i in ZLIB OPENSSL LIBPNG QT VIDALIA LIBEVENT TOR TOR_ALPHA FIREFOX MOZBUILD TORBUTTON HTTPSE HTTPSE_DEV OBFSPROXY
do
  URL=${i}"_URL"
  PACKAGE=${i}"_PACKAGE"
  wget -N --no-remove-listing ${!URL} >& /dev/null
  if [ $? -ne 0 ]; then
    echo "$i url ${!URL} is broken!"
    mv ${!PACKAGE} ${!PACKAGE}".removed"
    exit 1
  fi
done

# Get+verify sigs that exist
# XXX: This doesn't cover everything. See #8525
for i in TORBUTTON FIREFOX LIBEVENT TOR TOR_ALPHA VIDALIA OBFSPROXY OPENSSL
do
  URL=${i}"_URL"
  PACKAGE=${i}"_PACKAGE"
  if [ ! -f ${!PACKAGE}".asc" ]; then
    wget ${!URL}".asc" >& /dev/null
    if [ $? -ne 0 ]; then
      echo "$i GPG sig url ${!URL} is broken!"
      mv ${!PACKAGE} ${!PACKAGE}".nogpg"
      exit 1
    fi
  fi
  gpg ${!PACKAGE}".asc" >& /dev/null
  if [ $? -ne 0 ]; then
    echo "$i GPG signature is broken for ${!URL}"
    mv ${!PACKAGE} ${!PACKAGE}".badgpg"
    exit 1
  fi
done

# Record sha256sums
rm -f sha256sums.txt
for i in ZLIB OPENSSL LIBPNG QT VIDALIA LIBEVENT TOR TOR_ALPHA FIREFOX MOZBUILD TORBUTTON HTTPSE HTTPSE_DEV OBFSPROXY
do
  PACKAGE=${i}"_PACKAGE"
  sha256sum ${!PACKAGE} >> sha256sums.txt
done
